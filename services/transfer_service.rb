# frozen_string_literal: true

class TransferService
  def self.call(from_user_id, to_user_id, amount)
    User.trasaction do
      sender = obtain_user(from_user_id)
      receiver = obtain_user(to_user_id)
      sender.withdraw(amount)
      receiver.deposit(amount)
    end
  end

  private

  def obtain_user(id)
    User.lock!.find(id)
  end
end
