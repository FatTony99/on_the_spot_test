# frozen_string_literal: true

describe User do
  let(:balance) { 100 }
  subject { described_class.new(balance: balance) }

  describe 'validations' do
    it do
      is_expected.to validate_numericality_of(:balance)
        .is_greater_than_or_equal_to(0)
    end
  end

  describe '#deposit' do
    it { expect { subject.deposit(10) }.to change { subject.balance }.by(10) }
  end

  describe '#withdraw' do
    it { expect { subject.withdraw(10) }.to change { subject.balance }.by(-10) }

    it do
      subject.withdraw(100)
      expect(subject.valid?).to be false
    end
  end
end
