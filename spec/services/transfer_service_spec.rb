# frozen_string_literal: true

describe TransferService do
  describe '#call' do
    subject { vector.pos(*arguments) }

    let(:receiver) { User.create!(balance: 100) }
    let(:sender) { User.create!(balance: 100) }
    let(:amount) { 50 }
    subject { described_class.call(sender.id, receiver.id, amount) }

    context 'success' do
      it 'returns true' do
        expect { subject }.to be true
      end

      it 'transfers money' do
        expect { subject }.to change { sender.balance }.by(-amout)
          .and change { sender.balance }.by(amount)
      end
    end

    context 'failure' do
      let(:amount) { 200 }

      it 'returns false' do
        expect { subject }.to be false
      end

      it 'does not transfer' do
        sender.update!(balance: 10)
        expect { subject }.to_not change { sender.balance }
          .and change { sender.balance }
      end
    end
  end
end
